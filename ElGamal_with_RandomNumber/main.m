%main.m
%code for cryptosystem with ElGamal and a random integer value
%call functions for encryption and sending key and message
clear all

format long;

%generate public key
alice_keys = generate_keys();
public_key = alice_keys([1,2,3]);
s = alice_keys(4);

%encryption
[ciphertext,m] = encryption(public_key);
disp("message:");
disp(m);

%decryption
plaintext = decryption(ciphertext,s,public_key);

%compare message and decrypted_message
if m == plaintext
    disp("The decrypted message is the same as the original message")
else
    disp("The messages are not equal!")
end

%attack
attack = attack(public_key, ciphertext);
disp(attack)
% 
% %compare message and hacked plaintext
if m == attack
    disp("The hacked message is the same as the original message")
 else
     disp("The messages are not equal!")
 end

