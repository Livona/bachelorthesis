function alice_keys = generate_keys()

%  
% 1. Generate a large random prime N
%     
bits = 7; %change this for longer numbers
lower_bound = 1;
upper_bound = 2^bits - 1;
p = primes(upper_bound);
N = p(randi([lower_bound,length(p)]));
%random integer x such that x < N
x = randi([lower_bound, N-1]);

%
% 2. Generate a radnom integer s < N
%
s = randi([lower_bound, N-1]);

%Example values for testing
%N = 1749940627;
%x = 25749480;
%s = 7207480595;

%calculate A
A = chebyshev_mod(x, s, N);
 
% Alices public Key (x, N, A) and private key is s
public_key = [x , N, A]; %public key with a random number and the calculated chebyshev polynomial

alice_keys = [public_key, s];

end